#%Module
# vi:set filetype=tcl:

source /usr/projects/hpcsoft/utilities/lib/envmodules_header
source [file dirname $ModulesCurrentModulefile]/.utils

prereq		PrgEnv-cray PrgEnv-gnu PrgEnv-pgi PrgEnv-intel
prereq		cray-mpich
conflict	paraview

if { ![info exists ::env(CRAY_HDF5_VERSION)] } {
	module load cray-hdf5
}
if { ![info exists ::env(CRAY_CPU_TARGET)] } {
	puts stderr "Unable to determine target CPU.  Please load the appropriate craype-* target CPU module"
	exit 1
}

#local variables
set name        [moduleName]
set version     [moduleVersion]
set machine     [machineName]
set os          [systemOS]
set targetcpu	[targetArch]

# Create usage log entry
source /usr/projects/hpcsoft/utilities/lib/envmodules_logging

proc ModulesHelp {} {
        puts stderr "\tParaView v$version"
        puts stderr "\n\tThis module file will configure your environment for properly using ParaView $version."
}

module-whatis   "ParaView v$version"

# Determine the major version of paraview to populate the libdir path variable
set pvlibdir [ regexp -inline {^[0-9]+\.[0-9]+} "$version" ]

set mpi_flav cray-mpich
set mpi_fver $env(CRAY_MPICH2_VER)
switch $env(PE_ENV) {
	"INTEL" {
		set compiler "intel"
		set compiler_version $env(INTEL_VERSION)
	}
	"GNU" {
		set compiler "gcc"
		set compiler_version $env(GNU_VERSION)
	}
	"CRAY" {
		set compiler "cray"
		set compiler_version $env(CRAY_CC_VERSION)
	}
	"PGI" {
		set compiler "pgi"
		set compiler_version $env(PGI_VERSION)
	}
	default {
		puts stderr "Unsupported compiler"
		exit 1
	}
	set compiler_version [join [lrange [split $compiler_version .] 0 1] .]
}

#set prefix /usr/projects/hpcsoft/${os}/${machine}/${name}/${version}_${compiler}-${compiler_version}_${mpi_flav}-${mpi_fver}_${targetcpu}

# Retrieve all installed directories
set base_dir /usr/projects/hpcsoft/${os}/${machine}/${name}
set all_dirs [glob -nocomplain -tails -directory ${base_dir} -type d ${version}_${compiler}-*_${mpi_flav}-*_${targetcpu}]
if { $all_dirs != "" } {
	# Set the compiler version to the highest version not greater than the loaded compiler
	set all_comp_vers [lsort -command compare_version [lmap x $all_dirs { lindex [split [lindex [split $x _] 1] -] end }]]
	foreach v $all_comp_vers {
		if { [compare_version $v $compiler_version] > 0 } {
			break
		}
		set gen_comp_ver $v
	}

	# Set the mpi version to the highest version not greater than the loaded mpi
        set all_mpi_vers [lsort -command compare_version [lmap x $all_dirs { lindex [split [lindex [split $x _] 2] -] end }]]
	foreach v $all_mpi_vers {
		if { [compare_version $v $mpi_fver] > 0 } {
			break
		}
		set gen_mpi_ver $v
	}

}
if { ![info exists gen_comp_ver] || ![info exists gen_mpi_ver] } {
	puts stderr "ParaView $version is not available for currently loaded combination of compiler, mpi, and target CPU:"
	puts stderr "${compiler}/${compiler_version} ${mpi_flav}/${mpi_fver} ${targetcpu}\n"
	puts stderr "Please load a different combination and try again.  Available ParaView combinations are:"
	set dirs [glob -nocomplain -tails -directory ${base_dir} -type d ${version}_*]
	if { $dirs != "" } {
		foreach d $dirs {
			set ds [split $d _]
                        set ds_comp [split [lindex $ds 1] -]
			set ds_comp "[join [lrange $ds_comp 0 end-1] -]/[lindex $ds_comp end]"
                        set ds_mpi [split [lindex $ds 2] -]
			set ds_mpi "[join [lrange $ds_mpi 0 end-1] -]/[lindex $ds_mpi end]"
			puts stderr "[lindex $ds 0] ${ds_comp} ${ds_mpi} [lindex $ds 3]"
		}
	}
	exit 1;
}
set prefix ${base_dir}/${version}_${compiler}-${gen_comp_ver}_${mpi_flav}-${gen_mpi_ver}_${targetcpu}

set pvlibpath ${prefix}/lib/${name}-${pvlibdir}
prepend-path	PATH		${prefix}/bin
prepend-path	LD_LIBRARY_PATH	${pvlibpath}:${prefix}/lib:${prefix}/../../mesa/current/lib
setenv		ParaView_DIR	${prefix}/lib/cmake/${name}-${pvlibdir}
setenv		PARAVIEWLIBPATH	${pvlibpath}
setenv		PYTHONHOME	/usr/projects/hpcsoft/cle6.0/common/anaconda/2.1.0-python-2.7
setenv		PYTHONPATH	${pvlibpath}/site-packages:${pvlibpath}/site-packages/vtk

# Set OpenSWR threading defaults to 1 per process
if { !(([info exists ::env(KNOB_MAX_THREADS_PER_CORE)] || [info exists ::env(KNOB_MAX_CORES_PER_NUMA_NODE)]) || [info exists ::env(KNOB_MAX_NUMA_NODES)] || [info exists ::env(KNOB_MAX_WORKER_THREADS)]) } {
	setenv KNOB_MAX_WORKER_THREADS	1
}
